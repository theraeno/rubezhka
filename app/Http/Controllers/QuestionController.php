<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{
    public function index() {
        $questions = Question::get();
        return view('questions', ['questions' => $questions]);
    }

    public function create(Request $request) {
        $question = new Question();
        $question->question = $request->input('question');
        $question->save();
        return response()->json($question);
    }
}
