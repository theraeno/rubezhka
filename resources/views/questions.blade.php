<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Questions</title>
</head>
<body>
    <form id="mainForm" action="/questions" method="POST">
        {{csrf_field()}}
        <textarea name="question" id="questionText" cols="30" rows="10"></textarea>
        <input type="submit">
    </form>

    <div class="questions">
        @foreach($questions as $question)
            <p>{{$question->question}}</p>
        @endforeach
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
        $(function() {
            $('#mainForm').submit(function(e) {
                e.preventDefault();

                $.ajax({
                    method: 'POST',
                    url: '/questions',
                    data: $(this).serialize(),
                    success: function(data) {
                        $('#questionText').val('');
                        $('.questions').append('<p>' + data.question + '</p>')
                    }
                })
            })
        })
    </script>
</body>
</html>